// this file is unused.
document.addEventListener("DOMContentLoaded", () => {
  // Create the button
  const button = document.createElement("button");
  button.id = "open-modal-button";
  button.style.cssText = `
      position: fixed; 
      bottom: 20px; 
      right: 20px; 
      padding: 15px; 
      background-color: #FD6215;
      color: white;
      border: none;
      border-radius: 100%;
      cursor: pointer; 
      box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); 
      display: flex; 
      justify-content: center; 
      align-items: center; 
      z-index: 999`;

  button.innerHTML = `
      <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-message-circle-more">
        <path d="M7.9 20A9 9 0 1 0 4 16.1L2 22Z"/>
        <path d="M8 12h.01"/>
        <path d="M12 12h.01"/>
        <path d="M16 12h.01"/>
      </svg>`;

  // Create the modal container
  const modal = document.createElement("div");
  modal.id = "modal";
  modal.style.cssText = `
      display: none; 
      position: fixed; 
      bottom: 90px; 
      right: 20px; 
      height: 70vh;
      width: 400px; 
      background: white;
      border: .2px solid #E6E9EE;
      border-radius: 3px;
      box-shadow: 0 0 40px rgba(0, 0, 0, 0.1); 
      z-index: 1000`;

  // Create the modal content
  const modalContent = document.createElement("div");
  modalContent.id = "modal-content";
  modalContent.style.cssText = `
      width: 100%; 
      height: 100%; 
      border-radius: 3px;
      box-shadow: 0 0 20px rgba(0, 0, 0, 0.1); 
      text-align: center`;

  // Append the modal content to the modal
  modal.appendChild(modalContent);

  // Append the button and modal to the body
  document.body.appendChild(button);
  document.body.appendChild(modal);

  let iframeInitialized = false;

  // Event listener to open the modal
  button.addEventListener("click", () => {
    if (modal.style.display === "none") {
      modal.style.display = "flex";
      if (!iframeInitialized) {
        const iframe = document.createElement("iframe");

        // Add source depending on the environment
        // iframe.src = "http://chatbot.innocent-corp.jp/chatform/form"; // production
        iframe.src = window.location.href + "/form"; // development
        iframe.style.cssText =
          "width: 100%; height: 100%; border: none; border-radius: 3px;";

        modalContent.appendChild(iframe);
        iframeInitialized = true;
      }
    } else {
      modal.style.display = "none";
    }
  });

  const createCloseButton = () => {
    const closeButton = document.createElement("button");
    closeButton.textContent = "×";
    closeButton.style.cssText = `
      position: absolute;
      top: 3px;
      right: 5px;
      background: none;
      border: none;
      font-size: 21px;
      color: white;
      cursor: pointer;
      text-shadow: 1px 1px #ff0000;`;
    closeButton.addEventListener("click", () => {
      modal.style.display = "none";
    });
    return closeButton;
  };

  const closeButton = createCloseButton();
  modalContent.appendChild(closeButton);

  // Media query to adjust modal dimensions on mobile devices
  const mediaQuery = window.matchMedia("(max-width: 500px)");
  function handleMobileResize(e) {
    if (e.matches) {
      modal.style.height = "80vh";
      modal.style.width = "87vw";
      modal.style.borderRadius = "3px";
    } else {
      modal.style.width = "400px";
      modal.style.height = "70vh";
      modal.style.bottom = "90px";
      modal.style.right = "20px";
      modal.style.borderRadius = "3px";
    }
  }

  // Attach listener function on state changes
  mediaQuery.addEventListener("change", handleMobileResize);
  // Initial check
  handleMobileResize(mediaQuery);
});
