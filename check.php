<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header('Content-Type: application/json');

$rawData = file_get_contents('php://input');
$data = json_decode($rawData, true);

if (isset($data['form'])) {
    if ($data['form'] !== null) {
        $response = array(
            'status' => 'success',
            'data' => $data['form']
        );
        echo json_encode($response);
    } else {
        $response = array(
            'status' => 'error',
            'message' => 'Invalid JSON data received'
        );
        echo json_encode($response);

    }
} else {
    $response = array(
        'status' => 'error',
        'message' => 'No data received'
    );
    echo json_encode($response);
}

