<?php

    function getBasePath() {
        /* Since the environment is different when it's on production from development. (* This case we need to validate the url caused of none proper deploymnet procedure *)
           Function to get validate base URL */
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443 ? "https://" : "http://";
        return $protocol !== "http://" ?  '/chatform' : '.';
    }

    $basePath = getBasePath();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <style>
        /*  define colors to css  */
        :root {
            --base-color: #FD6215; /* header, inputs border  */
            --secondary-color: #FFF2D0; /* progress bar */
            --primary-color: #FF992E; /* active inputs, progress bar background */
            --tertiary-color: #FDE399; /* chat canvas */
        }

        /*  fade in animation keyframes */
        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        /*  with a duration of 0.3 seconds and an easing*/
        .fade-in {
            animation: fadeIn 0.3s ease-in-out;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 7px;
            height: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: var(--secondary-color);
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: var(--primary-color);
            border-radius: 5px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: var(--base-color);
        }

        /* Hide scrollbar for Chrome, Safari and Opera */
        .no-scrollbar::-webkit-scrollbar {
         display: none;
        }

        /* Hide scrollbar for IE, Edge and Firefox */
        .no-scrollbar {
            -ms-overflow-style: none; /* IE and Edge */
            scrollbar-width: none; /* Firefox */
        }

        /* button right bottom */
        #chat-button {
            position: fixed;
            bottom: 20px !important;
            right: 20px !important;
            padding: 15px !important;
            background-color: var(--base-color) !important;
            color: white !important;
            border: none !important;
            border-radius: 100% !important;
            cursor: pointer !important;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2) !important;
            display: flex !important;
            justify-content: center !important;
            align-items: center !important;
            z-index: 999 !important;
        }

        /* fixed dialog modal */
        #chat-dialog {
            display: none;
            position: fixed;
            border-radius: 3px !important;
            bottom: 90px !important;
            right: 20px !important;
            height: 700px !important;
            width: 370px !important;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2) !important;
            z-index: 1000 !important;
        }

        #chat-btn-closed {
            position: absolute !important;
            height: 25px !important;
            width: 25px !important;
            top: -14px !important;
            right: -9px !important;
            background: var(--base-color) !important;
            border: 1px solid white !important;
            font-size: 16px !important;
            color: white !important;
            cursor: pointer !important;
            border-radius: 100%;
            display: flex;
            align-items: center;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2) !important;
        }

        .bordered-theme {
            border: 2px solid var(--base-color);
        }

        @media screen and (max-width: 600px) {
            #chat-dialog {
                border-radius: 3px !important;
                bottom: 90px !important;
                right: 20px !important ;
                height: 80vh !important;
                width: 87vw !important;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2) !important;
                z-index: 1000 !important;
            }
        }

        @media screen and (max-height: 700px) {
            #chat-dialog {
                border-radius: 3px !important;
                bottom: 90px !important;
                right: 20px !important;
                height: 80vh !important;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2) !important;
                z-index: 1000 !important;
            }
        }

        @media screen and (max-height: 900px) {
            #chat-dialog {
                border-radius: 3px !important;
                bottom: 90px !important;
                right: 20px !important;
                height: 80vh !important;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2) !important;
                z-index: 1000 !important;
            }
        }

        .chat-container {
            display: flex !important;
            justify-content: center !important;
            height: 100% !important;
            width: 100% !important;
            border-radius: 5px !important;
            background-color: white !important;
        }

        .chat-content {
            display: flex !important;
            flex-direction: column !important;
            height: 100% !important; 
            width: 100% !important;
            position: relative !important;
        }

       
        .chat-header {
            height: 13%;
            position: relative !important;
            top: 0 !important;
            display: flex !important;
            flex-direction: column !important;
            width: 100% !important;
            background-color: var(--base-color) !important;
            border-top-right-radius: 3px !important;
            border-top-left-radius: 3px !important;
        
        }

        .chat-inner-header {
            display: flex !important;
            justify-content: center !important;
            height: 100% !important;
            flex-direction: column;
            align-items: center !important;
        }

        .chat-title-content {
            display: flex !important;
            flex-direction: column !important;
            max-width: 268px !important;
        }

        .chat-title-content h2 {
            color: white !important;
            font-weight: bolder !important;
            font-size: 1.8rem !important;
            text-align: center !important;
            text-shadow: 0 2px 4px rgba(0, 0, 0, 0.1) !important;
            margin: 0.5rem 0 0.5rem 0;
        }

        .chat-body {
            height: 87% !important;
            overflow-y: auto !important;
            display: flex;
            flex-direction: column !important;
            border: 1 !important;
            padding: 0.5rem 0.5rem 0.5rem 0.5rem !important;
            gap: 0.5rem !important;
            background-color: var(--tertiary-color) !important;
        }

        .chat-input {
            width: 100% !important; 
            overflow-y: auto !important;
            position: relative !important;
        }
        .chat-progress-content {
            width: 100% !important;
            position: relative !important;
        }

        .progress-container {
            width: 100% !important;
            position: relative !important;
        }
        .progress-background {
            display: flex;
            align-items: center;
            height: 1.7rem !important;
            width: 100% !important;
            background-color: var(--secondary-color) !important;
        }

        .progress-bar {
            height: 100% !important;
            width: 0%;
            background-color: var(--primary-color);
        }

        .progress-label {
            font-size: 1.3rem !important;
            font-weight: 300 !important;
            text-shadow: 0 2px 4px rgba(0, 0, 0, 0.1) !important;
            position: absolute !important;
            right: 0 !important;
            margin-right: 0.5rem !important;
            color: var(--base-color) !important;
            display: inline-flex !important;
            top: -2px;
        }

        .control-error {
            width: 100% !important;
            font-size: 14px !important;
            text-align: center !important;
            color: #FCFCFC !important;
        }

        .typing-img {
            object-fit: cover !important;
            object-position: left !important;
            width: 40px !important;
            height: 20px !important;
            border-radius: 100px !important;
            background-color: transparent !important;
        }

        .csr-img-container {
            background-color: #ffffff !important;
            width: 33px !important;
            height: 33px !important;
            border-width: 1px !important;
            user-select: none !important;
            border-radius: 100% !important;
            margin-right: 0.5rem !important;
        }

        .csr-img-avatar {
            object-fit: cover !important;
            object-position: left !important;
            width: 100% !important;
            height: 100% !important;
            border-radius: 100% !important;
        }

        .control-list-order div {
            display: flex !important;
            flex-direction: column !important;
      
        }
       
        .control-list-order p {
            font-size: 1rem !important;
            color: #5D5E62 !important;
            line-height: 14px !important;
        }

        .prompt-container {
            display: flex !important;
            flex-direction: column !important;
            margin-bottom: 1.3rem !important;
        }

        .prompt-inner {
            display: flex !important;
            align-items: end !important;
            margin-top: 2px;
        }

        .prompt-container-list {
            -ms-flex: 1 !important; 
            flex: 1 !important;
            overflow: hidden !important;
        }

        .prompt-list {
            display: flex !important;
            flex-direction:column !important;
            gap: 0.2rem !important;
          
        }

        .prompt {
            padding: 0.6rem !important;
            background-color: #ffffff !important;
            color: #374151 !important;
            line-height: 1.4rem !important;
            font-size: 1.2rem !important;
        }
       
        .prompt-c-1 {
            border-top-left-radius: 1rem !important;
            border-top-right-radius: 1rem !important; 
            border-bottom-right-radius: 1rem !important; 
        }

        .prompt-c-2 {
            border-bottom-right-radius: 1rem !important;
            border-bottom-left-radius: 1rem !important; 
            border-top-right-radius: 1rem !important;  

        }

        .prompt-c-3 {
            border-top-right-radius: 1rem !important;
            border-bottom-right-radius: 1rem !important; 
        }

        .prompt-max-w {
            width: max-content !important; 
        }

        .prompt-min-w {
            width: 14rem !important; 
        }

        .prompt-label {
            color: #374151 !important;
            font-size: 1.2rem !important;
            line-height: 1.5rem !important;
            font-weight: 500 !important;
            text-align: center !important;
        }

        .prompt-response-container {
            display: flex !important; 
            justify-content: flex-end !important; 
        }

        .prompt-response-text {
            padding: .5rem !important;
            color: #374151 !important;
            font-size: 1.2rem !important;
            line-height: 1.5rem !important;
            max-width: 20rem !important;
            border-radius: 1rem !important;
            border-top-right-radius: 0 !important;
            border-width: 2px !important;
            background: #E7F1FA !important;
            border: 1px solid #72BDFD !important;
        }

        .fieldset-container {
            display: grid !important;
            gap: 0.5rem !important; 
            padding-bottom: 0.5rem !important; 
            border: none !important;
        }

        .fieldset-grid-2 {
            grid-template-columns: repeat(2, minmax(0, 1fr)) !important; 
        }

        .fieldset-grid-3 {
            grid-template-columns: repeat(3, minmax(0, 1fr)) !important; 
        }

        .fieldset-grid-4 {
            grid-template-columns: repeat(4, minmax(0, 1fr)) !important; 
        }   

        .fieldset-container .radio-container {
            display: flex !important;
            padding-top: 0.5rem !important;
            padding-bottom: 0.5rem !important;
            padding-left: 0.75rem !important;
            padding-right: 0.75rem !important;
            background-color: #ffffff !important;
            justify-content: center !important;
            align-items: center !important;
            height: 3rem !important;
            border-radius: 5.5rem !important;
            border-width: 2px !important;
            cursor: pointer !important;
            box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05) !important;
            transition: all 200ms ease-in-out !important;
        }

        .radio-container:has(:checked) {
            border-color: var(--base-color) !important;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1) !important;
            background-color: var(--secondary-color) !important;
        }

        .radio-container:hover {
            background-color: var(--secondary-color) !important;
            border-color: var(--base-color) !important;
        }

        .hidden {
            display:none !important;
        }
        .form-input-container {
            display: flex !important; 
           
            gap: 0.5rem !important; 
        }
        .form-input-button {
            padding: 0.5rem !important;
            color: #ffffff !important;
            font-size: 1rem !important;
            line-height: 1rem !important;
            border-radius: 0.5rem !important;
            box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05) !important;
            background-color: var(--base-color) !important;
            border: 2px solid var(--base-color) !important;
            outline: none !important;
            height: 3.2rem !important;
        }

       .form-input-container textarea, input, select {
            padding-top: 0.5rem !important;
            padding-bottom: 0.5rem !important;
            padding-left: 0.75rem !important;
            padding-right: 0.75rem !important;
            font-size: 1.2rem !important;
            line-height: 1rem !important;
            flex: 1 1 0% !important;
            border-radius: 0.5rem !important;
            border: 2px solid var(--base-color) !important;
            outline: none !important;
        }

        .form-input-container select {
            background: white url("<?= $basePath ?>/assets/img/chevron-down.png") no-repeat scroll calc(97% + 3px) center/1.2rem auto;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            font-size: 1.2rem !important;
            height: 3rem !important;
            line-height: 1.8rem !important;
        }

        .form-completion-container {
            display: flex !important;
            margin-top: 0.25rem !important;
            margin-bottom: 1.25rem !important;
            justify-content: center !important;
        }

        .form-completion-container #form-send-button {
            display: flex !important;
            transition-property: background-color, border-color, color, fill, stroke !important;
            color: #ffffff !important;
            justify-content: center !important;
            align-items: center !important;
            height: 4.8rem !important;
            border-radius: 9999px !important;
            cursor: pointer !important;
            box-shadow: 0 15px 21px -2px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, -0.95) !important;
            background: var(--base-color) !important;
            outline: none !important;
            border-color: 2px solid var(--primary-color) !important;
            outline: auto !important;
            padding: 0 1.5rem 0 1.5rem !important;
        }

        #form-send-button:hover {
            background: var(--primary-color) !important;
            border: 2px solid var(--base-color) !important;
        }

        #form-send-button label {
            font-size: 1.125rem !important;
            line-height: 1.75rem !important;  
            font-weight: 600 !important; 
            text-align: center !important; 
            cursor: pointer !important; 
            text-decoration: drop-shadow(0 4px 3px rgba(0, 0, 0, 0.07)) drop-shadow(0 2px 2px rgba(0, 0, 0, 0.06)) !important; 
        }

        .active-label {
            color: #FFFFFF !important;
            text-decoration: drop-shadow(0 4px 3px rgba(0, 0, 0, 0.07)) drop-shadow(0 2px 2px rgba(0, 0, 0, 0.06)) !important; 
        }

    </style>
</head>
<body>
    <button id="chat-button">
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-message-circle-more">
            <path d="M7.9 20A9 9 0 1 0 4 16.1L2 22Z"/>
            <path d="M8 12h.01"/>
            <path d="M12 12h.01"/>
            <path d="M16 12h.01"/>
        </svg>
    </button>

    <div id="chat-dialog">
        <div class="chat-container">
            <div class="chat-content">
                <div class="chat-header">
                    <div class="chat-inner-header">
                        <div class="chat-title-content">
                            <h2>ショッピングフレーム現金リクエストチャットフォーム</h2>
                        </div>
                    </div>
                    <div class="progress-container">
                        <div class="progress-background">
                            <div id="form-progress" class="progress-bar">
                                <label id="form-step" class="progress-label"></label>
                            </div>
                        </div>
                    </div>
                    <button type="button" id="chat-btn-closed">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-x"><path d="M18 6 6 18"/><path d="m6 6 12 12"/></svg>
                    </button>
                </div>
                <div class="chat-body" id="form-chat"></div>                
                <div class="chat-input" id="form-control"></div>    
            </div>
        </div>
    </div>
    <?php
    $display = 1;
    // accept control type [ "radio", "text", "number", "email", "select", "textarea", "tel" ]
    $formConfig = [
        [
            "prompt" => [
                "ご応募にあたっていくつかご質問がございます。（下記フォームに必要事項をご記入・選択してください。）", 
                "内容によってはご連絡までにお時間がかかる場合がございますので、予めご了承ください。", 
                "必要な情報を入力してください", 
                "クレジットカードは お持ちですか？",
            ], 
            "control" => [ 
                "type" => "radio", 
                "field" => "is_card",
                "value" => [ 
                    ["label" => "有り", "value" => 1], 
                    ["label" => "無し", "value" => 0],
                ], 
                "rules" => [
                    "accept" => [1],
                    "error" => [
                        "クレジットカードをお持ちでない方はお申し込みできません"
                    ]
                ]
            ],
            "order" => [
                "※当サービスのご利用には、ご契約クレジットカードのショッピング枠の残高が必要となります。", 
                "※デビットカード、その他プリペイトカードはご利用いただくことはできません。", 
                "※現在「メルペイスマート払い」「Kyash」はご利用いただく事はできません。"
            ],
        ],
        [
            "prompt" => ["クレジットカード名義"], 
            "control" => [ 
                "type" => "radio", 
                "field" => "cardholder",
                "value" => [ 
                    ["label" => "本人名義", "value" => 0], 
                    ["label" => "家族名義", "value" => 1],
                    ["label" => "本人・家族名義以外", "value" => 2]
                ],
                "rules" => [
                    "accept" => [0],
                    "error" => [
                        "ご本人様名義以外はご利用できません"
                    ]
                ]
            ],
            "order" => ["※当サービスをご利用できるのは、クレジットカードご契約者ご本人様のみとなります。"]
        ],
         [
            "prompt" => ["カードの種類選択"], 
            "control" => [ 
                "type" => "radio", 
                "field" => "cardtype",
                "value" => [ 
                    ["label" => "クレジットカード", "value" => 1], 
                    ["label" => "paidy", "value" => 2],
                    ["label" => "バンドル", "value" => 3],
                    ["label" => "auペイ", "value" => 4],
                    ["label" => "メルペイ", "value" => 5],
                    ["label" => "kyash", "value" => 7],
                    ["label" => "ペイペイ", "value" => 8],
                    ["label" => "LINE PAY", "value" => 9],
                    ["label" => "myac", "value" => 10],
                    ["label" => "ソフトバンク", "value" => 11],
                    ["label" => "その他", "value" => 6],
                ],
                "rules" => [
                    "accept" => [1, 2, 3, 4, 8, 9, 10, 11, 6],
                    "error" => [
                        "メルペイは現在ご利用できません",
                        "kyashは現在ご利用できません"
                    ]
                ]
            ]
        ],
        [
            "prompt" => ["希望金額"], 
            "control" => [ 
                "type" => "number", 
                "field" => "amount",
                "value" => "",
                "placeholder" => "1",
                "pattern" => "^[1-9][0-9]*$"
            ],
            "order" => ["万円"],
            "response" => [
                "text" => "ご希望の金額は {0}",
                "format" => ["currency"]
            ]
        ],
        [
            "prompt" => ["メールアドレス"], 
            "control" => [ 
                "type" => "email", 
                "field" => "mail_address",
                "value" => "",
                "placeholder" => "customer@donnatokimo-c.com"
            ]
        ] ,
        [
            "prompt" => ["電話番号"], 
            "control" => [ 
                "type" => "tel", 
                "field" => "tel1",
                "value" => "",
                "placeholder" => "電話番号を入力してください",
                "pattern" => "\d{2,4}-?\d{3,4}-?\d{3,4}"
            ],
            "response" => [ 
                "text" => "私の電話番号です {0}"
            ]
        ],
        [
            "prompt" => ["お名前を教えていただけますか"], 
            "control" => [ 
                "type" => "text",
                "field" => "name_1", 
                "value" => "",
                "placeholder" => "お名前を入力してください"
            ]
        ],
        [
            "prompt" => ["ふりがな"], 
            "control" => [ 
                "type" => "text", 
                "field" => "read_1",
                "value" => "",
                "placeholder" => "ふりがなを入力してください",
                "pattern" => "[\u3041-\u3096]*"
            ]
        ],
        [
            "prompt" => ["連絡希望時間帯"], 
            "control" => [ 
                "type" => "radio", 
                "field" => "time",
                "value" => [
                    ["label" => "9:00-10:00", "value" => 2], 
                    ["label" => "10:00-11:00", "value" => 3],
                    ["label" => "11:00-12:00", "value" => 4],
                    ["label" => "12:00-13:00", "value" => 5],
                    ["label" => "13:00-14:00", "value" => 6],
                    ["label" => "14:00-15:00", "value" => 7],
                    ["label" => "15:00-16:00", "value" => 8],
                    ["label" => "16:00-17:00", "value" => 9],
                    ["label" => "17:00-18:00", "value" => 10],
                ],
                "placeholder" => "Please select"
            ],
            "order" => [
                "※初回のみお電話対応が必須となります。", 
                "※営業時間外の場合は翌営業日午前9時以降にご連絡させて頂きます。", 
                "【営業時間9:00～18:00（無休）】"
            ]
        ],
        [
            "prompt" => ["郵便番号"], 
            "control" => [ 
                "type" => "text", 
                "field" => "postl",
                "value" => "",
                "placeholder" => "1500001"
            ]
        ],
        [
            "prompt" => ["都道府県"], 
            "control" => [ 
                "type" => "select", 
                "field" => "addr1",
                "value" => [
                    ["label" => "北海道", "value" => 1],
                    ["label" => "青森県", "value" => 2],
                    ["label" => "岩手県", "value" => 3],
                    ["label" => "宮城県", "value" => 4],
                    ["label" => "秋田県", "value" => 5],
                    ["label" => "山形県", "value" => 6],
                    ["label" => "福島県", "value" => 7],
                    ["label" => "茨城県", "value" => 8],
                    ["label" => "栃木県", "value" => 9],
                    ["label" => "群馬県", "value" => 10],
                    ["label" => "埼玉県", "value" => 11],
                    ["label" => "千葉県", "value" => 12],
                    ["label" => "東京都", "value" => 13],
                    ["label" => "神奈川県", "value" => 14],
                    ["label" => "新潟県", "value" => 15],
                    ["label" => "富山県", "value" => 16],
                    ["label" => "石川県", "value" => 17],
                    ["label" => "福井県", "value" => 18],
                    ["label" => "山梨県", "value" => 19],
                    ["label" => "長野県", "value" => 20],
                    ["label" => "岐阜県", "value" => 21],
                    ["label" => "静岡県", "value" => 22],
                    ["label" => "愛知県", "value" => 23],
                    ["label" => "三重県", "value" => 24],
                    ["label" => "滋賀県", "value" => 25],
                    ["label" => "京都府", "value" => 26],
                    ["label" => "大阪府", "value" => 27],
                    ["label" => "兵庫県", "value" => 28],
                    ["label" => "奈良県", "value" => 29],
                    ["label" => "和歌山県", "value" => 30],
                    ["label" => "鳥取県", "value" => 31],
                    ["label" => "島根県", "value" => 32],
                    ["label" => "岡山県", "value" => 33],
                    ["label" => "広島県", "value" => 34],
                    ["label" => "山口県", "value" => 35],
                    ["label" => "徳島県", "value" => 36],
                    ["label" => "香川県", "value" => 37],
                    ["label" => "愛媛県", "value" => 38],
                    ["label" => "高知県", "value" => 39],
                    ["label" => "福岡県", "value" => 40],
                    ["label" => "佐賀県", "value" => 41],
                    ["label" => "長崎県", "value" => 42],
                    ["label" => "熊本県", "value" => 43],
                    ["label" => "大分県", "value" => 44],
                    ["label" => "宮崎県", "value" => 45],
                    ["label" => "鹿児島", "value" => 46],
                    ["label" => "沖縄県", "value" => 47],
                ],
                "placeholder" => "選択してください"
            ],
        ],
        [
            "prompt" => ["ご住所 (番地をご入力ください)"], 
            "control" => [ 
                "type" => "text", 
                "field" => "addr2",
                "value" => "",
                "placeholder" => "市区町村＋番地"
            ]
        ],
        [
            "prompt" => ["建物名"], 
            "control" => [ 
                "type" => "text",
                "field" => "addr3", 
                "value" => "",
                "placeholder" => "建物名"
            ]
        ],
        [
            "prompt" => ["メッセージ"], 
            "control" => [ 
                "type" => "textarea", 
                "field" => "contents", 
                "value" => "",
                "placeholder" => "ご質問・ご要望等お気軽にお書きください。"
            ]
        ],
        [
            "prompt" => ["当社が 金融業者でない事を 理解していますか？"], 
            "control" => [ 
                "type" => "radio", 
                "field" => "op", 
                "value" => [
                    ["label" => "はい", "value" => 1],
                    ["label" => "いいえ", "value" => 0]
                ],
                "rules" => [
                    "accept" => [1],
                    "error" => [
                        "当社は金融業者ではありません"
                    ]
                ],
                "placeholder" => "",
            ],
        ]
    ];

    if ($display == 1) {
        // unset($formConfig[1]);
        $formConfig = array_values($formConfig);
    }
 ?>
    <script>
        $(()=>{
            
        // let form = [
        //     { 
        //         prompt: [
        //             "ご応募にあたっていくつかご質問がございます。（下記フォームに必要事項をご記入・選択してください。）", 
        //             "内容によってはご連絡までにお時間がかかる場合がございますので、予めご了承ください。", 
        //             "必要な情報を入力してください", 
        //             "クレジットカードは お持ちですか？",
        //         ], 
        //         control: { 
        //             type: "radio", 
        //             field: "is_card",
        //             value: [ 
        //                 { label: "有り", value: 1}, 
        //                 { label: "無し", value: 0},
        //             ], 
        //             rules: {
        //                 accept: [1],
        //                 error: [
        //                     "クレジットカードをお持ちでない方はお申し込みできません"
        //                 ]
        //             }
        //         },
        //         order: [
        //             "※当サービスのご利用には、ご契約クレジットカードのショッピング枠の残高が必要となります。", 
        //             "※デビットカード、その他プリペイトカードはご利用いただくことはできません。", 
        //             "<p class='text-red-500 font-bold text-sm'>※現在「メルペイスマート払い」「Kyash」はご利用いただく事はできません。</p>"
        //         ],
        //     },
        //     { 
        //         prompt: ["クレジットカード名義"], 
        //         control: { 
        //             type: "radio", 
        //             field: "cardholder",
        //             value: [ 
        //                 { label: "本人名義", value: 0 }, 
        //                 { label: "家族名義", value: 1 },
        //                 { label: "本人・家族名義以外", value: 2 }
        //             ],
        //             rules: {
        //                 accept: [0],
        //                 error: [
        //                     "ご本人様名義以外はご利用できません"
        //                 ]
        //             }
        //         },
        //         order: ["※当サービスをご利用できるのは、クレジットカードご契約者ご本人様のみとなります。"]
        //     },
        //     { 
        //         prompt: ["カードの種類選択"], 
        //         control: { 
        //             type: "radio", 
        //             field: "cardtype",
        //             value: [ 
        //                 { label: "クレジットカード", value: 1}, 
        //                 { label: "paidy", value: 2},
        //                 { label: "バンドル", value: 3},
        //                 { label: "auペイ", value: 4},
        //                 { label: "メルペイ", value: 5},
        //                 { label: "kyash", value: 7},
        //                 { label: "ペイペイ", value: 8},
        //                 { label: "LINE PAY", value: 9},
        //                 { label: "myac", value: 10},
        //                 { label: "ソフトバンク", value: 11},
        //                 { label: "その他", value: 6},
        //             ],
        //             rules: {
        //                 accept: [1,2,3,4,8,9,10,11,6],
        //                 error: [
        //                     "メルペイは現在ご利用できません",
        //                     "kyashは現在ご利用できません"
        //                 ]
        //             }
        //         }
        //     },
        //     { 
        //         prompt: ["希望金額"], 
        //         control: { 
        //             type: "number", 
        //             field: "amount",
        //             value: "",
        //             placeholder: "1",
        //             pattern: "^[1-9][0-9]*$"
        //         },
        //         order: ["万円"],
        //         response: {
        //             text: "ご希望の金額は {0}",
        //             format: [ "currency" ]
        //         }
        //     },
        //     {
        //         prompt: ["メールアドレス"], 
        //         control: { 
        //             type: "email", 
        //             field: "mail_address",
        //             value: "",
        //             placeholder: "customer@donnatokimo-c.com"
        //         }
        //     },
        //     { 
        //         prompt: ["電話番号"], 
        //         control: { 
        //             type: "tel", 
        //             field: "tel1",
        //             value: "",
        //             placeholder: "電話番号を入力してください",
        //             pattern: "\d{2,4}-?\d{3,4}-?\d{3,4}"
        //         },
        //         response: { 
        //             text: `私の電話番号です {0}`
        //         },
            
        //     },
        //     { 
        //         prompt: ["お名前を教えていただけますか"], 
        //         control: { 
        //             type: "text",
        //             field: "name_1", 
        //             value: "",
        //             placeholder: "お名前を入力してください"
        //         }
        //     },
        //     { 
        //         prompt: ["ふりがな"], 
        //         control: { 
        //             type: "text", 
        //             field: "read_1",
        //             value: "",
        //             placeholder: "ふりがなを入力してください",
        //             pattern: "[\u3041-\u3096]*"
        //         }
        //     },
        
        //     { 
        //         prompt: ["連絡希望時間帯"], 
        //         control: { 
        //             type: "radio", 
        //             field: "time",
        //             value: [
        //                 {label:  "9:00-10:00", value: 2}, 
        //                 { label: "10:00-11:00", value: 3},
        //                 { label: "11:00-12:00", value: 4},
        //                 { label: "12:00-13:00", value: 5},
        //                 { label: "13:00-14:00", value: 6},
        //                 { label: "14:00-15:00", value: 7},
        //                 { label: "15:00-16:00", value: 8} ,
        //                 { label: "16:00-17:00", value: 9},
        //                 { label: "17:00-18:00", value: 10},
        //             ],
        //             placeholder: "Please select"
        //         },
        //         order: [
        //             "※初回のみお電話対応が必須となります。", 
        //             "※営業時間外の場合は翌営業日午前9時以降にご連絡させて頂きます。", 
        //             "【営業時間9:00～18:00（無休）】"
        //         ]
        //     },
        //     { 
        //         prompt: ["郵便番号"], 
        //         control: { 
        //             type: "text", 
        //             field: "postl",
        //             value: "",
        //             placeholder: "1500001"
        //         }
        //     },
        //     { 
        //         prompt: ["都道府県"], 
        //         control: { 
        //             type: "select", 
        //             field: "addr1",
        //             value: [
        //                 { label: "北海道", value: 1 },
        //                 { label: "青森県", value: 2 },
        //                 { label: "岩手県", value: 3 },
        //                 { label: "宮城県", value: 4 },
        //                 { label: "秋田県", value: 5 },
        //                 { label: "山形県", value: 6 },
        //                 { label: "福島県", value: 7 },
        //                 { label: "茨城県", value: 8 },
        //                 { label: "栃木県", value: 9 },
        //                 { label: "群馬県", value: 10 },
        //                 { label: "埼玉県", value: 11 },
        //                 { label: "千葉県", value: 12 },
        //                 { label: "東京都", value: 13 },
        //                 { label: "神奈川県", value: 14 },
        //                 { label: "新潟県", value: 15 },
        //                 { label: "富山県", value: 16 },
        //                 { label: "石川県", value: 17 },
        //                 { label: "福井県", value: 18 },
        //                 { label: "山梨県", value: 19 },
        //                 { label: "長野県", value: 20 },
        //                 { label: "岐阜県", value: 21 },
        //                 { label: "静岡県", value: 22 },
        //                 { label: "愛知県", value: 23 },
        //                 { label: "三重県", value: 24 },
        //                 { label: "滋賀県", value: 25 },
        //                 { label: "京都府", value: 26 },
        //                 { label: "大阪府", value: 27 },
        //                 { label: "兵庫県", value: 28 },
        //                 { label: "奈良県", value: 29 },
        //                 { label: "和歌山県", value: 30 },
        //                 { label: "鳥取県", value: 31 },
        //                 { label: "島根県", value: 32 },
        //                 { label: "岡山県", value: 33 },
        //                 { label: "広島県", value: 34 },
        //                 { label: "山口県", value: 35 },
        //                 { label: "徳島県", value: 36 },
        //                 { label: "香川県", value: 37 },
        //                 { label: "愛媛県", value: 38 },
        //                 { label: "高知県", value: 39 },
        //                 { label: "福岡県", value: 40 },
        //                 { label: "佐賀県", value: 41 },
        //                 { label: "長崎県", value: 42 },
        //                 { label: "熊本県", value: 43 },
        //                 { label: "大分県", value: 44 },
        //                 { label: "宮崎県", value: 45 },
        //                 { label: "鹿児島", value: 46 },
        //                 { label: "沖縄県", value: 47 },
        //             ],
        //             placeholder: "選択してください"
        //         },
        //     },
        //     { 
        //         prompt: ["ご住所 (番地をご入力ください)"], 
        //         control: { 
        //             type: "text", 
        //             field: "addr2",
        //             value: "",
        //             placeholder: "市区町村＋番地"
        //         }
        //     },
        //     { 
        //         prompt: ["建物名"], 
        //         control: { 
        //             type: "text",
        //             field: "addr3", 
        //             value: "",
        //             placeholder: "建物名"
        //         }
        //     },
        //     { 
        //         prompt: ["メッセージ"], 
        //         control: { 
        //             type: "textarea", 
        //             field: "contents", 
        //             value: "",
        //             placeholder: "ご質問・ご要望等お気軽にお書きください。"
        //         }
        //     },
        //     { 
        //         prompt: ["当社が 金融業者でない事を 理解していますか？"], 
        //         control: { 
        //             type: "radio", 
        //             field: "op", 
        //             value: [
        //                 { label: "はい", value: 1 },
        //                 { label: "いいえ", value: 0 }
        //             ],
        //             rules: {
        //                 accept: [1],
        //                 error: [
        //                     "当社は金融業者ではありません"
        //                 ]
        //             },
        //             placeholder: "",
        //         },
        //     },
        // ];

        let form = <?php echo json_encode($formConfig); ?>;

        let chatCanvas = $('#form-chat'), 
            fieldSet = $('#form-control'),  
            arrResponse = [];
        var initIndex = form[0];
            
        // asynchronous helper
        const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

        // function handle inputs and generative prompts
        const formIndex = (index) => {
            const v = form[index];
            return {
                // this function allow to get all message with merging based on form prompt object.
                prompts: (arr = []) => {
                    const mergeArray = arr.length === 0 ? v.prompt : (typeof index === "undefined") ? arr : [...arr,  ...v.prompt];
                    if(!Array.isArray(mergeArray)) return [];
                    return mergeArray;
                },

                // this function render and validate the field set instructions based on the form array object structure.
                controls: () => {
                    let html = "";
                    if(Object.keys(v.control).length === 0) return;
                    const { value, type, placeholder, field: asFieldName } = v.control;
                    const acceptTypes  = ["radio", "text", "number", "email", "select", "textarea", "tel"];

                      // check the property control type if the accept types is match.
                    if(!acceptTypes.includes(type)) return `<p class="control-error">何か問題が発生しました。次の解析制御はアンマップされます [${t.join(', ')}]</p>`;
                    
                    let errObjectType = `<label class="control-error">値のタイプにより、コントロール タイプが無効です。</label>`;

                    switch(type) {
                        case "radio":
                            const gridStyle = value.length > 13  ? value.length > 20 ? `.fieldset-grid-4` : `.fieldset-grid-3`  : `fieldset-grid-2`;
                            // this case render radio button  field
                            html += `<fieldset class="fieldset-container ${gridStyle} fade-in">`;
                            html +=  value instanceof Array ? value.map((item,idx)=>{ 
                                return `<label
                                            for="${asFieldName+idx}"
                                            class="radio-container bordered-theme"
                                            >
                                            <div>
                                                <p class="prompt-label">${item.label}</p>
                                            </div>
                                            <input 
                                                type="${type}" 
                                                id="${asFieldName+idx}" 
                                                name="${asFieldName}" 
                                                class="form-control-input hidden sr-only " 
                                                field-id="${index}" 
                                                value="${item.value}" 
                                            />
                                        </label>` ;
                                }).join('') : errObjectType ;
                            html += `</fieldset>`;
                            break;
                   
                        case "textarea":
                                // this case render textarea field
                            html += `<div class="form-input-container">
                                        <textarea 
                                            id="${asFieldName}" 
                                            rows="5" 
                                            name="${asFieldName}" 
                                            class="form-control-input"
                                            field-id="${index}" 
                                            placeholder="${placeholder}" 
                                        ></textarea>
                                        <button 
                                            type="button"
                                            class="form-input-button"
                                            >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-send"><path d="m22 2-7 20-4-9-9-4Z"/><path d="M22 2 11 13"/></svg>
                                        </button>
                                    <div>`;
                                break;
                        case "select": 
                            // this case render select options field
                            html +=  `<div class="form-input-container" style="flex-direction: column;" >
                                        <select 
                                            name="${asFieldName}" 
                                            class="form-control-input"
                                            field-id="${index}" 
                                            placeholder="${placeholder}"
                                        >
                                        <option value="">選択してください</option>`;
                                        html += value.map((item,idx)=>{
                                            return `<option value="${item.value}">${item.label}</option>`;
                                        }).join('');
                            html += `   </select> 
                                     <div>` ;
                            break;   
                        default: 
                            // this case render of type [text, number, email, number] input field
                            html += typeof value  === 'string' 
                                        ?  `<div class="form-input-container">
                                                <input 
                                                    type="${type}" 
                                                    id="${asFieldName}" 
                                                    name="${asFieldName}" 
                                                    field-id="${index}" 
                                                    class="form-control-input"
                                                    value="${value}" 
                                                    placeholder="${placeholder}" 
                                                    pattern="${typeof v?.control?.pattern !== "undefined" && v?.control?.pattern}" 
                                                />
                                                <button 
                                                    type="button"
                                                    class="form-input-button">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-send"><path d="m22 2-7 20-4-9-9-4Z"/><path d="M22 2 11 13"/></svg>
                                                </button>
                                            <div>`  
                                        : errObjectType;
                            break;    
                        }
                        // with this code allow to render all the order instruction details if it has object order
                        if(typeof v.order !== "undefined" && v.order.length > 0) {
                            const orderList = v.order;
                            html += `<div class="control-list-order">`;
                            html += orderList.map((item,idx)=>`<p>${item}</p>`).join('');
                            html += `</div>`
                        }
                        return html;
                    },

                    // this function allow to render all promps in csr side.
                    getPrompt: async (arr = []) => {
                        const mergeArray = formIndex(index).prompts(arr),
                        delay = 500; 
                        let html = `<div class="prompt-container fade-in" data-index="${index}">
                                            <div class="prompt-inner">
                                                <div class="csr-img-container">
                                                    <img src="<?= $basePath ?>/assets/img/csr.png" alt="customer support" loading="lazy" class="csr-img-avatar" />
                                                </div>
                                                <div class="prompt-container-list">
                                                    <div class="prompt-list" id="prompt-list-${index}"></div>
                                                </div>
                                            </div>
                                        </div>`;
                        chatCanvas.append(html); 
                        for (let idx = 0; idx < mergeArray.length; idx++) {
                            const item = mergeArray[idx];
                            const widthClasses = item.length < 20 ? 'prompt-max-w' : 'prompt-min-w';

                            // add loading state ui 
                            const loadingHtml = `<div id="loading-${index}-${idx}">
                                                    <img src="<?= $basePath ?>/assets/img/typing.gif" loading="lazy" alt="typing" class="typing-img"/>
                                                </div>`;
                                
                            $(`#prompt-list-${index}`).append(loadingHtml);

                            chatCanvas.animate({ scrollTop: chatCanvas[0].scrollHeight }, 'slow');
                            
                            // add delay for every each of prompt on csr side
                            await sleep(delay);

                            // let formattedTime = new Date().toLocaleTimeString('ja-JP', { hour12: false });                            
                            const promptHtml = `<div 
                                                    id="prompt-${index}-${idx}" 
                                                    class="prompt fade-in ${idx === 0 ? `prompt-c-1` : idx == (mergeArray.length - 1) ? 'prompt-c-2' : `prompt-c-3` }  ${widthClasses}"
                                                >
                                                    ${item} 
                                                </div>`;

                            // remove loading ui and append the incomming prompt                    
                            $(`#loading-${index}-${idx}`).replaceWith(promptHtml);
                        }
                    }
                }
            }

            const renderedInitPrompt = async () => {
                let _index = form.indexOf(initIndex);
                $('#form-step').text(`${_index}/${form.length}`);

                const htmlPrompt = await formIndex(_index).getPrompt();
                await sleep(500);
                const control = await formIndex(_index).controls();

                chatCanvas.append(htmlPrompt);

                let lastAppendedDiv = chatCanvas.children().last();
                lastAppendedDiv.append(fieldSet);

                fieldSet.html(control).css('marginTop', '0.8rem');
                fieldSet.find('input, button, textarea').first().focus();
            }

            // for initial click load of chat btutton, append controls, prompts and progress percentage
            let isRendered = false;
    
            $(document).on('click', '#chat-button', async () => {
               const dialog = $("#chat-dialog");
                if(dialog.css("display") == 'none'){
                    dialog.show();
                    if (!isRendered) {  
                        // set the flag to true after rendering
                        isRendered = true; 
                        await renderedInitPrompt();
                    }
                }    
            });

          
            $(document).ready(async ()=>{
                // show the dialog upon page reload
                $("#chat-dialog").show();
                if (!isRendered) {  
                    isRendered = true; 
                    await renderedInitPrompt()
                }
            })

            // hide dialog function
            $(document).on('click', '#chat-btn-closed', () => {
                $("#chat-dialog").hide();
            });

            // validate function for all input control values
            const isValidatedField = (type, value) => {
                const trimmedValue = value.trim();
                if (trimmedValue.length === 0) return false;
                switch (type) {
                    case 'number':
                        if (isNaN(trimmedValue)) return false;
                        break;
                    case 'email':
                        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                        if (!emailRegex.test(trimmedValue)) return false;
                        break;
                    case 'tel':
                        const telRegex = /^\+?[0-9\s\-().]{7,15}$/;
                        if (!telRegex.test(trimmedValue)) return false;
                        break;
                    default:
                        break;
                }
                return true;
            }


            // format string into currency
            String.prototype.formatToCurrency = function(format, currency) {
                return new Intl.NumberFormat(format, {
                    style: 'currency',
                    currency,
                    minimumFractionDigits: 0
                }).format(this);
            };

            // transpose the response value from lable object prop
            const trp = (param,  args = null) => {
                return Array.isArray(param) ? param.find((x) => x.value == args).label : args
            } 

            // generate response based on the constrol input object options
            const generateResponse =  (template, values) => {           
                const  { control } = template;
                //check if has object "response"
                if(typeof template.response === 'undefined') {
                    if(control.value instanceof Array) {
                        return trp(control.value, values);
                    }else {
                        return values;
                    }
                }else {
                    const { response } = template;
                    const result =  response?.text?.replace(/{(\d+)}/g, (match, number) => {
                        let val = values[number];
                        if(typeof response.format !== 'undefined') {
                            const lFormat = typeof response.format !== 'undefined' ? response.format : [];
                            lFormat.forEach((v, idx)=>{
                                // case string conversion
                                switch(v) {
                                    case "currency": 
                                        val = values[number].formatToCurrency('ja-JP', 'JPY');
                                    break;
                                    default:
                                        val = values[number];
                                    break;
                                }
                            });
                        }else {
                            val = trp(control.value, values);
                        }
                        return typeof values[number] !== 'undefined' ?  val : match;
                    });
                    return result;
                }
            };



            $(document).on('change', '.form-control-input', async (e) => {
                const { value, type, name } = e.target;

                // validate input
                if (!isValidatedField(type, value)) {
                    e.preventDefault();
                    return;
                }

                // get and increment the current index
                let index = $(e.currentTarget).attr('field-id');
                let isValidRules = false;

                
                // get validate rules
                if(typeof form[index].control.rules !== 'undefined') {
                    const { accept } = form[index].control.rules;
                    const f = accept.some((v) => v == value );
                    if(!f) {
                        isValidRules = false;
                    }else {
                        isValidRules = true;
                    }
                }else { 
                    isValidRules = true;
                }

                // update response array
                if(isValidRules){
                    // const newObj = { [name]: { fieldIndex: index, value } };
                    const newObj = { fieldIndex: index, name, value } 
                    // Update arrResponse using spread syntax
                    arrResponse = [...arrResponse, newObj];   
                    
                    // Update progress bar and label
                    updateProgressBar(+index + 1);
                }
            
                // Generate response message
                const responseMessage = generateResponse(form[index], [value]);

                // update index for new prompt
                const newIndex =  +index + 1;

                // check if has a next prompt
                const isNext = newIndex !== form.length;

                // add to customer sides inputs
                appendToChatCanvas(responseMessage);

                // check if there's no additional prompt object in the form array
                if (!isNext) {
                    setTimeout(() => {
                        scrollToBottom(chatCanvas);
                    }, 500);
                }

                // get the next prompt
                const initIndex = form[newIndex];
                let _index = form.indexOf(initIndex);

                // transposed response
                const trpResponse = trp(form[index].control.value, value);
                
                // generate new prompts and controls
                const newPrompts = [trpResponse, `有難うございます。 ${isNext ? '次は' : ''}`];
            
                // prevent choosing multiple values at the same time
                disableInputs(true);

                if(!isValidRules) {
                    // render the csr error validation
                    await handleErrorPrompt(index, [trpResponse, "他のオプションを選択してください。"]);
                }else {
                    // check if it has form controls ended and render submit button
                    if (typeof initIndex === "undefined" && _index === -1 ) {
                        await handleFormCompletion(trpResponse);
                        return clearFieldSet();
                    }else {
                        await handleNextPrompt(_index, newPrompts);
                    }   
                
                }

                // enabled the control rendered
                disableInputs(false);

                // scroll to bottom when the rendered chat canvas change
                scrollToBottom(chatCanvas);
            });

            // Function to append message to chat canvas
            const appendToChatCanvas = (message) => {
                chatCanvas.append(`<div class="prompt-response-container">
                                        <div class="prompt-response-text">
                                            ${message}
                                        </div>
                                    </div>`);
                scrollToBottom(chatCanvas)
            }

            // Function to clear the fieldset
            const clearFieldSet = () => {
                fieldSet.hide().html('').fadeIn('slow');
            }

            // Function to scroll to the bottom of the chat canvas
            const scrollToBottom = (element) => {
                element.animate({ scrollTop: element[0].scrollHeight }, 'slow');
            }

            // Function to update the progress bar
            const updateProgressBar = (newIndex) => {  
                const formLength = form.length
                const percentage = Math.round(newIndex / form.length * 100);

                // update the step label
                $('#form-step').text(`${newIndex}/${formLength}`).toggleClass('active-label', percentage > 90);

                //change the progress bar width
                $('#form-progress').css('width', percentage+'%');
            }

            // Function to handle form completion
            const handleFormCompletion = async (trpResponse) => {
                const htmlPrompt = await formIndex().getPrompt(
                    [trpResponse, 
                    "続行するには、フォーム送信ボタンをクリックしてください。", 
                    "どうもありがとうございます!"]);
                chatCanvas.append(htmlPrompt);
                chatCanvas.append(`<div class="form-completion-container">
                                        <button type="button" id="form-send-button">
                                            <label>フォームを送信する</label>
                                        </button>
                                    </div>`);
            }

            // Function to disable or enable inputs
            const disableInputs = (disabled) => {
                $('.form-control-input').prop('disabled', disabled);
            }

            // Function to handle the next prompt
            const handleNextPrompt = async (_index, newPrompts) => {
                await sleep(700);
                const htmlPrompt = await formIndex(_index).getPrompt(newPrompts);
                await sleep(700);
                const control = await formIndex(_index).controls();
                let lastAppendedDiv = chatCanvas.children().last();
                lastAppendedDiv.append(fieldSet);

                fieldSet.html(control).css('marginTop', '1rem');
                fieldSet.find('input, button, textarea').first().focus();
            }

            // Function to render error validation rules
            const handleErrorPrompt = async (index, newPrompts) => {
                const randStr = (Math.random() + 1).toString(36).substring(7);
                const delay = 500;
                chatCanvas.append(`<div class="prompt-container fade-in">
                                        <div class="prompt-inner">
                                            <div class="csr-img-container">
                                                <img src="<?= $basePath ?>/assets/img/csr.png" alt="customer support" loading="lazy" class="csr-img-avatar" />
                                            </div>
                                            <div class="prompt-container-list">
                                                <div class="prompt-list" id="prompt-list-${randStr}"></div>
                                            </div>
                                        </div>
                                    </div>`);
                // get objects rules
                const { accept: formRules, error } = form[index].control.rules;

                // combine all prompts with error rules                        
                const mergeArray = [...newPrompts, ...error];
                
                for (let idx = 0; idx < mergeArray.length; idx++) {
                    const item = mergeArray[idx];
                    const widthClasses = item.length < 20 ? 'prompt-max-w' : 'prompt-min-w';
                    // add loading state ui 
                    const loadingHtml = `<div id="loading-${randStr}-${idx}">
                                            <img src="<?= $basePath ?>/assets/img/typing.gif" loading="lazy" alt="typing" class="typing-img"/>
                                        </div>`;
                    $(`#prompt-list-${randStr}`).append(loadingHtml);

                    chatCanvas.animate({ scrollTop: chatCanvas[0].scrollHeight }, 'slow');

                    // add delay for every each of prompt on csr side
                    await sleep(delay);

                    const promptHtml = `<div 
                                            id="prompt-${index}-${idx}" 
                                            class="prompt ${idx === 0 ? `prompt-c-1` : idx == (mergeArray.length - 1) ? 'prompt-c-2' : `prompt-c-3` }  ${widthClasses}"
                                        >
                                            ${item} 
                                        </div>`;

                    // remove loading ui and append the incomming prompt                    
                    $(`#loading-${randStr}-${idx}`).replaceWith(promptHtml);
                }    

                // append the last control
                const control = await formIndex(index).controls();
                let lastAppendedDiv = chatCanvas.children().last();
                lastAppendedDiv.append(fieldSet);

                fieldSet.html(control);
                fieldSet.find('input, button, textarea').first().focus();
            
            }

            // Serialize all values and submit
            $(document).on('click', '#form-send-button', (e) => {
                e.stopPropagation();
                $.ajax({
                    url: window.location.href + "<?= $basePath ?>/check.php",
                    method: 'POST',
                    data: JSON.stringify({ form: arrResponse }), 
                    contentType: 'application/json',
                    success: (response) => {
                        const { status, data } = response
                        console.log(data)
                    },
                    error: (error) => {
                        console.error(error);
                    }
                });
            });

        });
    </script>
</body>
</html>